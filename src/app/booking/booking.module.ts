import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BookingRoutingModule} from './booking-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {BookingMainComponent} from './pages/booking-main/booking-main.component';
import {BookingMapComponent} from './pages/booking-map/booking-map.component';
import {SharedModule} from '../@shared/shared.module';
import {BookingMapThreeComponent} from './pages/booking-map-three/booking-map-three.component';
import {BookingMapTwoComponent} from './pages/booking-map-two/booking-map-two.component';
import {NgxAutocomPlaceModule} from 'ngx-autocom-place';


@NgModule({
  declarations: [BookingMainComponent, BookingMapComponent, BookingMapTwoComponent, BookingMapThreeComponent],
  imports: [
    CommonModule,
    BookingRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxAutocomPlaceModule,
  ],
  exports: [BookingMainComponent],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {floatLabel: 'always'},
    },
  ],
})
export class BookingModule {
}
