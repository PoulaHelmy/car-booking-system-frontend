import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';

interface MarkerRoute {
  id: number;
  slug: string;
  routes: any;
}

interface RouteRequest {
  origin: any;
  destination: any;
  travelMode: string;
}

@Component({
  selector: 'app-booking-map-three',
  templateUrl: './booking-map-three.component.html',
  styleUrls: ['./booking-map-three.component.scss']
})
export class BookingMapThreeComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('mapContainer', {static: false}) gmap: ElementRef;
  @Input() selectedRoute: any;
  map: google.maps.Map;
  directionsRenderer = new google.maps.DirectionsRenderer();
  directionsService = new google.maps.DirectionsService();
  lat = 30.0444196;
  lng = 31.2357116;

  // Coordinates to set the center of the map
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 15,
    scrollwheel: true
  };

  ALLAvailableRoutes: MarkerRoute[] = [
    {
      id: 0,
      slug: 'new-york',
      routes: [
        {
          position: new google.maps.LatLng(30.033333, 31.233334),
          title: 'Cairo, Cairo Governorate, Egypt'
        },
        {
          position: new google.maps.LatLng(30.0074, 31.4913),
          title: 'owntown mall, new cairo city, cairo governorate, egypt'
        },
      ]
    },
    {
      id: 1,
      slug: '',
      routes: []
    },
    {
      id: 2,
      slug: '',
      routes: []
    },
  ];

  // tslint:disable-next-line:typedef
  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
  }

  constructor() {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    // console.log(this.ALLAvailableRoutes[0].routes[0]);
  }

  ngAfterViewInit(): void {
    this.mapInitializer();
  }


  mapInitializer(): void {

    this.map = new google.maps.Map(this.gmap.nativeElement, {
      center: this.coordinates,
      zoom: 20,
      scrollwheel: true
    });
    this.directionsRenderer.setMap(this.map);
    const request = {
        origin: this.ALLAvailableRoutes[0].routes[0].position,
        destination: this.ALLAvailableRoutes[0].routes[1].position,
        travelMode: 'DRIVING'
      }
    ;
    // @ts-ignore
    // tslint:disable-next-line:only-arrow-functions typedef
    // this.directionsService.route(request, function(result, status) {
    //   if (status === google.maps.DirectionsStatus.OK) {
    //     this.directionsRenderer.setDirections(result);
    //     console.log(result);
    //   }
    // });


  }// end of mapInitializer

  loadAllMarkers(markers): void {
    markers.forEach(markerInfo => {
      // Creating a new marker object
      const marker = new google.maps.Marker({
        ...markerInfo
      });

      // creating a new info window with markers info
      const infoWindow = new google.maps.InfoWindow({
        content: marker.getTitle()
      });

      // Add click event to open info window on marker
      marker.addListener('click', () => {
        infoWindow.open(marker.getMap(), marker);
      });

      // Adding marker to google map
      marker.setMap(this.map);
    });
  }

  // tslint:disable-next-line:typedef
  getRouteMarkers(markers: MarkerRoute[], index: number) {
    markers.forEach((marker) => {
      if (marker.id === index) {
        return marker;
      }
    });
  }

  // tslint:disable-next-line:typedef
  sendRequest(request: RouteRequest, callback) {
    // @ts-ignore
    this.directionsService.route(request, callback);
  }

  // tslint:disable-next-line:typedef
  // getRoutePointsAndWaypoints(Points) {
  //   if (Points.length <= 10) {
  //     this.drawRoutePointsAndWaypoints(Points);
  //   } else {
  //     const newPoints = new Array();
  //     const startPoint = Points.length - 10;
  //     const Legs = Points.length - 10;
  //     for (let i = startPoint; i < Points.length; i++) {
  //       newPoints.push(Points[i]);
  //     }
  //     this.drawRoutePointsAndWaypoints(newPoints);
  //     drawPreviousRoute(Legs);
  //   }
  // }

  // tslint:disable-next-line:typedef
  // drawRoutePointsAndWaypoints(Points) {
  //   // Define a variable for waypoints.
  //   const _waypoints = new Array();
  //
  //   if (Points.length > 2) // Waypoints will be come.
  //   {
  //     for (const j = 1; j < Points.length - 1; j++) {
  //       const address = Points[j];
  //       if (address !== '') {
  //         _waypoints.push({
  //           location: address,
  //           stopover: true  // stopover is used to show marker on map for waypoints
  //         });
  //       }
  //     }
  //     // Call a drawRoute() function
  //     this.drawRoute(Points[0], Points[Points.length - 1], _waypoints);
  //   } else if (Points.length > 1) {
  //     // Call a drawRoute() function only for start and end locations
  //     this.drawRoute(Points[_mapPoints.length - 2], Points[Points.length - 1], _waypoints);
  //   } else {
  //     // Call a drawRoute() function only for one point as start and end locations.
  //     this.drawRoute(Points[_mapPoints.length - 1], Points[Points.length - 1], _waypoints);
  //   }
  // }

  // drawRoute() will help actual draw the route on map.
  // tslint:disable-next-line:typedef
  // drawRoute(originAddress, destinationAddress, waypoints) {
  //   // Define a request variable for route .
  //   let request = '';
  //
  //   // This is for more then two locatins
  //   if (waypoints.length > 0) {
  //     request = {
  //       origin: originAddress,
  //       destination: destinationAddress,
  //       waypoints, // an array of waypoints
  //       optimizeWaypoints: true, // set to true if you want google to determine the shortest route or false to use the order specified.
  //       travelMode: 'DRIVING'
  //     };
  //   } else {
  //     // This is for one or two locations. Here noway point is used.
  //     request = {
  //       origin: originAddress,
  //       destination: destinationAddress,
  //       travelMode: 'DRIVING'
  //     };
  //   }
  //
  //   // This will take the request and draw the route and return response and status as output
  //   // @ts-ignore
  //   this.directionsService.route(request, function(response, status) {
  //     if (status === google.maps.DirectionsStatus.OK) {
  //       this.directionsRenderer.setDirections(response);
  //     }
  //   });
  // }
}// end of Class


///////////////////////////////////////////
// // Default Marker
// defaultMarker = new google.maps.Marker({
//   position: this.coordinates,
//   map: this.map,
//   title: 'Hello World!'
// });

// markers = [
//   {
//     position: new google.maps.LatLng(30.1068, 31.3671),
//     map: this.map,
//     title: 'city stars , egypt'
//   },
//   {
//     position: new google.maps.LatLng(30.0074, 31.4913),
//     map: this.map,
//     title: 'downtown mall, new cairo city, cairo governorate, egypt'
//   }
// ];
