import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-booking-map',
  templateUrl: './booking-map.component.html',
  styleUrls: ['./booking-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookingMapComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('mapContainer', {static: false}) gmap: ElementRef;
  @Input('originLocation') originLocation: string;
  @Input('destinationLocation') destinationLocation: string;
  @Input('wayPoints') wayPoints: any = [];
  @Output() resultDisTime: EventEmitter<any> = new EventEmitter();
  map: google.maps.Map;
  directionsService = new google.maps.DirectionsService();
  directionsRenderer = new google.maps.DirectionsRenderer();
  distanceMatrix = new google.maps.DistanceMatrixService();
  lat = 30.044281;
  lng = 31.340002;
  // Coordinates to set the center of the map
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 8,
    scrollwheel: true,
  };

  // tslint:disable-next-line:typedef
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.calculateAndDisplayRoute(
      this.directionsService,
      this.directionsRenderer
    );
  }

  constructor() {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.mapInitializer();
  }

  mapInitializer(): void {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.directionsRenderer.setMap(this.map);
    // Adding Click event to default marker
  } // end of mapInitializer
  // tslint:disable-next-line:typedef
  returnWayPoints(points) {
    console.log(points);
    if (points === undefined) {
      return [];
    }
    return points;
  }

  // tslint:disable-next-line:typedef
  calculateAndDisplayRoute(
    directionsService: google.maps.DirectionsService,
    directionsRenderer: google.maps.DirectionsRenderer
  ) {
    const that = this;
    console.log('##########');
    const waypts: google.maps.DirectionsWaypoint[] = [];
    const checkboxArray = this.wayPoints;
    // tslint:disable-next-line:triple-equals
    if (checkboxArray.length > 0 && this.wayPoints != undefined) {
      // tslint:disable-next-line:prefer-for-of
      checkboxArray.forEach((elm) => {
        waypts.push({
          location: elm,
          stopover: true,
        });
      });
    }
    if (this.originLocation !== '' && this.destinationLocation !== '') {
      directionsService.route(
        {
          origin: this.returnWayPoints(this.originLocation),
          destination: this.returnWayPoints(this.destinationLocation),
          // tslint:disable-next-line:triple-equals
          waypoints: this.returnWayPoints(waypts),
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING,
        },
        (response, status) => {
          if (status === 'OK') {
            directionsRenderer.setDirections(response);
            const route = response.routes[0];
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        }
      );
    }
    // @ts-ignore
    this.distanceMatrix.getDistanceMatrix(
      {
        travelMode: google.maps.TravelMode.DRIVING,
        origins: [this.originLocation, ...this.wayPoints],
        destinations: [this.destinationLocation],
        unitSystem: google.maps.UnitSystem.METRIC,
        // transitOptions: TransitOptions,
        // drivingOptions: DrivingOptions,
        // unitSystem: UnitSystem,
        // avoidHighways: Boolean,
        // avoidTolls: Boolean,
      },
      (response, status) => {
        if (status !== google.maps.DistanceMatrixStatus.OK) {
          window.alert('Error was: ' + status);
        } else {
          const origins = response.originAddresses;
          for (let i = 0; i < origins.length; i++) {
            const results = response.rows[i].elements;
            // tslint:disable-next-line:prefer-for-of
            for (let j = 0; j < results.length; j++) {
              const element = results[j];
              if (element.status === 'OK') {
                that.resultDisTime.emit({
                  time: element.duration.text,
                  distance: element.distance,
                  newTime: element.duration.value / 60
                });
              }
            }
          }
        }
      }
    );
  }// end of calculateAndDisplayRoute

  // tslint:disable-next-line:typedef
  setOutPut(value) {
    this.resultDisTime.emit(value);
  }
} // end of Class
