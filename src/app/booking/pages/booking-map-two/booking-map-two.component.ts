import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';

@Component({
  selector: 'app-booking-map-two',
  templateUrl: './booking-map-two.component.html',
  styleUrls: ['./booking-map-two.component.scss']
})
export class BookingMapTwoComponent implements OnInit, AfterViewInit, OnChanges {
  @ViewChild('mapContainer', {static: false}) gmap: ElementRef;
  // tslint:disable-next-line:no-input-rename
  @Input('lat') lat: any;
  // tslint:disable-next-line:no-input-rename
  @Input('lng') lng: any;
  public map: google.maps.Map;
  public bounds = new google.maps.LatLngBounds();
  latt = 30.0444196;
  lngg = 31.2357116;
  coordinates = new google.maps.LatLng(this.latt, this.lngg);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 15,
    scrollwheel: true
  };

  // tslint:disable-next-line:typedef
  ngOnChanges(changes: SimpleChanges) {

    if (!changes.lat.firstChange && !changes.lng.firstChange) {
      this.createMarker();
      console.log(changes);
    }
  }

  constructor() {
  }// end of constructor

  // tslint:disable-next-line:typedef
  ngOnInit() {
  }// end of ngOnInit

  ngAfterViewInit(): void {
    this.mapInitializer();
  }// end of ngAfterViewInit
  mapInitializer(): void {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
  }// end of mapInitializer
  // tslint:disable-next-line:typedef
  createMarker() {
    this.mapInitializer();
    const Marker = new google.maps.Marker({
      position: new google.maps.LatLng(this.lat, this.lng),
    });
    // Adding default marker to map
    Marker.setMap(this.map);
    // this.bounds = new google.maps.LatLngBounds();
    this.bounds.extend(Marker.getPosition());
    console.log('ffffff');
    this.map.fitBounds(this.bounds);
    console.log('@@@@@@@@@@@@@@2222');
  }
}// end of Class
