import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation,} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators,} from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {CarsService} from '../../../@core/services/cars.service';
import {TimesDatesService} from '../../../@core/services/times-dates.service';
import {MyMapsService} from '../../../@core/services/my-maps.service';
import {CountriesService} from '../../../@core/services/countries.service';


@Component({
  selector: 'app-booking-main',
  templateUrl: './booking-main.component.html',
  styleUrls: ['./booking-main.component.scss'],
  // Encapsulation has to be disabled in order for the
  // component style to apply to the select panel.
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {displayDefaultIndicatorType: false},
    },
  ],
})
export class BookingMainComponent implements OnInit {
  @ViewChild('searchMap', {static: false}) searchMap: google.maps.places.SearchBox;
  @ViewChild('mapTwo', {static: false}) mapTwo: ElementRef;

  // tslint:disable-next-line:constiable-name
  constructor(private formBuilder: FormBuilder,
              private carsService: CarsService,
              private timeDateService: TimesDatesService,
              private myMapsService: MyMapsService,
              private countryService: CountriesService) {
  }

  /****************** FORM INITIALIZER ************************/
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  originLocationItem: FormGroup;
  /****************** DATA Arrays ************************/
  AllCars = this.carsService.getAllCars();
  timesPickers = this.timeDateService.getAllDayHoursPicker();
  hoursPickers = this.timeDateService.getHalfDayTime();
  fullHoursPickers = this.timeDateService.getALlDayTime();
  countries = this.countryService.getAllCountries();
  availbaleRoutes = this.myMapsService.getAvailableRoutes();
  vehicles = this.carsService.getAllvehicles();
  public defaultTabIndex = 0;
  selectedTime: string;
  selectedHour: string;
  options;
  mapTwoLat;
  mapTwoLng;
  mapThree = {
    origin: '',
    destination: '',
    wayPoints: [],
  };
  firstMapInputs = {
    origin: '',
    destination: '',
    wayPoints: [],
    distance: '0 Km',
    time: '0 H 0 M',
  };
  points = [];

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.initalizeForms();
    (this.firstFormGroup.controls.distanceFormGroup as FormGroup).get('extraHours').valueChanges
      .subscribe((res) => {
        console.log(this.firstMapInputs.time);
      });
  }// end of ngOnInit
  /****************** initalizeForms ************************/
  // tslint:disable-next-line:typedef
  initalizeForms() {
    this.firstFormGroup = this.formBuilder.group({
      distanceFormGroup: new FormGroup({
        dateSelected: new FormControl(),
        timeSelected: new FormControl(),
        originLocation: new FormControl(),
        destinationLocation: new FormControl(),
        extraHours: new FormControl(0),
        locations: this.formBuilder.array([]),
      }),
      hourlyFormGroup: new FormGroup({
        dateSelected: new FormControl(),
        timeSelected: new FormControl(),
        destinationLocation: new FormControl(),
        extraOptionsLocation: new FormControl(),
        duration: new FormControl(0),
      }),
      flatRateFormGroup: new FormGroup({
        dateSelected: new FormControl(),
        timeSelected: new FormControl(),
        routeSelected: new FormControl(this.availbaleRoutes[0].value),
        transferType: new FormControl('one-way'),
        extraHours: new FormControl(0),
      }),
    });
    this.secondFormGroup = this.formBuilder.group({
      selectedCar: new FormControl(),
      childSeat: new FormControl(1),
      bouquetOfFlowers: new FormControl(1),
      vodkaBottle: new FormControl(1),
      frenchChampagne: new FormControl(1),
      alcoholPackage: new FormControl(0),
      airPortAssistance: new FormControl(0),
      bodyGuardService: new FormControl(0),
    });
    this.thirdFormGroup = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: [''],
      phone: [''],
      comments: [''],
    });

  }

  /****************** setFormControlValue ************************/
  // tslint:disable-next-line:typedef
  setFormControlValue(formGroup, controlName, value) {
    this.getFormControl(formGroup, controlName).setValue(value);
  }

  /****************** getFormControl ************************/
  // tslint:disable-next-line:typedef
  getFormControl(formGroup, controlName) {
    return formGroup.get(controlName);
  }

  /****************** Get ORIGINS LOCATIONS************************/
  // tslint:disable-next-line:typedef
  getAllLocations() {
    return (this.firstFormGroup.controls.distanceFormGroup as FormGroup).get(
      'locations'
    ) as FormArray;
  }

  /****************** add ORIGINS LOCATIONS************************/
  // tslint:disable-next-line:typedef
  addLocation() {
    this.originLocationItem = this.formBuilder.group({
      location: new FormControl('', [Validators.required]),
    });
    this.getAllLocations().push(this.originLocationItem);
  }

  /****************** remove  ORIGINS LOCATIONS************************/
  // tslint:disable-next-line:typedef
  removeLocation(index: number) {
    this.getAllLocations().removeAt(index);
    this.points.splice(index, 1);
    this.firstMapInputs.wayPoints = this.points.slice();
  }

  /****************** Get One ORIGINS LOCATIONS************************/
  // tslint:disable-next-line:typedef
  getOneLocation(index) {
    return this.getAllLocations().at(index);
  }

  /****************** changeTab ************************/
  // tslint:disable-next-line:typedef
  public changeTab(e) {
    this.defaultTabIndex = e;
  }

  /****************** changeTab ************************/
  // tslint:disable-next-line:typedef
  selectCar(id) {
    const Elms = document.querySelectorAll(`[id^=single_car_]`);
    Elms.forEach((elm, index) => {
      elm.classList.remove('activeSelectBTN');
      elm.innerHTML = `SELECT`;
      if (index === id - 1) {
        elm.classList.add('activeSelectBTN');
        this.secondFormGroup.controls.selectedCar.setValue(id);
      }
    });
  }

  /****************** SETTER FIRST FORM LOCATIONS ************************/
  // tslint:disable-next-line:typedef
  distanceOriginLocations(mapPredictionResults) {
    this.firstMapInputs.origin = mapPredictionResults.formatted_address;
    this.setFormControlValue(
      this.firstFormGroup.controls.distanceFormGroup,
      'originLocation',
      mapPredictionResults.formatted_address
    );
  }

  // tslint:disable-next-line:typedef
  distanceLocations(mapPredictionResults, index) {
    this.setFormControlValue(
      this.getOneLocation(index),
      'location',
      mapPredictionResults.formatted_address
    );
    this.points.push(mapPredictionResults.formatted_address);
    this.firstMapInputs.wayPoints = this.points.slice();
  }

  // tslint:disable-next-line:typedef
  placeChangedOneDestination(mapPredictionResults) {
    this.firstMapInputs.destination = mapPredictionResults.formatted_address;

    // tslint:disable-next-line:max-line-length
    this.setFormControlValue(
      this.firstFormGroup.controls.distanceFormGroup,
      'destinationLocation',
      mapPredictionResults.formatted_address
    );
  }

  /****************** END OF SETTER FIRST FORM LOCATIONS ************************/

  /****************** SETTER SECOND FORM LOCATIONS ************************/
  // tslint:disable-next-line:typedef
  placeChangedHourlyLocation(mapPredictionResults) {
    this.mapTwoLat = mapPredictionResults.geometry.location.lat();
    this.mapTwoLng = mapPredictionResults.geometry.location.lng();
    // console.log(mapPredictionResults);
    // console.log(mapPredictionResults.geometry.location.lat());
    // console.log(mapPredictionResults.geometry.location.lng());
    this.setFormControlValue(
      this.firstFormGroup.controls.hourlyFormGroup,
      'destinationLocation',
      mapPredictionResults.formatted_address
    );
  }

  // tslint:disable-next-line:typedef
  placeChangedHourlyExtraLocation(mapPredictionResults) {
    // tslint:disable-next-line:max-line-length
    this.setFormControlValue(
      this.firstFormGroup.controls.hourlyFormGroup,
      'extraOptionsLocation',
      mapPredictionResults.formatted_address
    );
  }

  /****************** END OF SETTER SECOND FORM LOCATIONS ************************/

  // tslint:disable-next-line:typedef
  submitFormONE() {
    console.log(this.firstFormGroup.value);
  }

  // tslint:disable-next-line:typedef
  submitSecForm() {
    console.log(this.secondFormGroup.value);
  }

  // tslint:disable-next-line:typedef
  setTimeDistance(event) {
    console.log(event);
    this.firstMapInputs.distance = event.distance.text;
    this.firstMapInputs.time = event.time;
  }
} // end of Class
