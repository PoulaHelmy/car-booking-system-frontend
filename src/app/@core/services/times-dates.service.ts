import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TimesDatesService {
  constructor() {
  }

  // tslint:disable-next-line:typedef
  getAllDayHoursPicker() {
    return [
      {label: '6:00', value: '6:00'},
      {label: '6:30', value: '6:30'},
      {label: '7:00', value: '7:00'},
      {label: '7:30', value: '7:30'},
      {label: '8:00', value: '8:00'},
      {label: '8:30', value: '8:30'},
      {label: '9:00', value: '9:00'},
      {label: '9:30', value: '9:30'},
      {label: '10:00', value: '10:00'},
      {label: '10:30', value: '10:30'},
      {label: '11:00', value: '11:00'},
      {label: '12:00', value: '12:00'},
      {label: '12:30', value: '12:30'},
      {label: '13:00', value: '13:00'},
      {label: '13:30', value: '13:30'},
      {label: '14:00', value: '14:00'},
      {label: '14:30', value: '14:30'},
      {label: '15:00', value: '15:00'},
      {label: '15:30', value: '15:30'},
      {label: '16:00', value: '16:00'},
      {label: '16:30', value: '16:30'},
      {label: '17:00', value: '17:00'},
      {label: '17:30', value: '17:30'},
      {label: '18:00', value: '18:00'},
      {label: '18:30', value: '18:30'},
      {label: '19:00', value: '19:00'},
      {label: '20:30', value: '20:30'},
      {label: '20:00', value: '20:00'},
      {label: '21:30', value: '21:30'},
      {label: '21:00', value: '21:00'},
      {label: '22:30', value: '22:30'},
      {label: '22:00', value: '22:00'},
      {label: '23:00', value: '23:00'},
      {label: '23:30', value: '23:30'},
      {label: '24:00', value: '24:00'},
    ];
  }

  // tslint:disable-next-line:typedef
  getHalfDayTime() {
    return [
      {label: '1 hour(s)', value: 1},
      {label: '2 hour(s)', value: 2},
      {label: '3 hour(s)', value: 3},
      {label: '4 hour(s)', value: 4},
      {label: '5 hour(s)', value: 5},
      {label: '6 hour(s)', value: 6},
      {label: '7 hour(s)', value: 7},
      {label: '8 hour(s)', value: 8},
      {label: '9 hour(s)', value: 9},
      {label: '10 hour(s)', value: 10},
      {label: '11 hour(s)', value: 11},
      {label: '12 hour(s)', value: 12},
    ];
  }

  // tslint:disable-next-line:typedef
  getALlDayTime() {
    return [
      {label: '0 hour(s)', value: 0},
      {label: '1 hour(s)', value: 1},
      {label: '2 hour(s)', value: 2},
      {label: '3 hour(s)', value: 3},
      {label: '4 hour(s)', value: 4},
      {label: '5 hour(s)', value: 5},
      {label: '6 hour(s)', value: 6},
      {label: '7 hour(s)', value: 7},
      {label: '8 hour(s)', value: 8},
      {label: '9 hour(s)', value: 9},
      {label: '10 hour(s)', value: 10},
      {label: '11 hour(s)', value: 11},
      {label: '12 hour(s)', value: 12},
      {label: '13 hour(s)', value: 13},
      {label: '14 hour(s)', value: 14},
      {label: '15 hour(s)', value: 15},
      {label: '16 hour(s)', value: 16},
      {label: '17 hour(s)', value: 17},
      {label: '18 hour(s)', value: 18},
      {label: '19 hour(s)', value: 19},
      {label: '20 hour(s)', value: 20},
      {label: '21 hour(s)', value: 21},
      {label: '22 hour(s)', value: 22},
      {label: '23 hour(s)', value: 23},
      {label: '24 hour(s)', value: 24},
    ];
  }
} // end of CLass
