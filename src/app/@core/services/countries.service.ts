import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {
  constructor() {
  }

  // tslint:disable-next-line:typedef
  getAllCountries() {
    return [
      'England',
      'U.S',
      'France',
      'Espain',
      'Angola',
      'Italy',
      'Germany',
      'Russia',
      'Bahrain',
    ];
  }
}

// end of CLass
