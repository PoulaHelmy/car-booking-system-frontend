import {Injectable} from '@angular/core';
import {Cars} from '../../@shared/models/cars';

@Injectable({
  providedIn: 'root',
})
export class CarsService {
  constructor() {
  }

  getAllCars(): Array<Cars> {
    return [
      {
        id: 1,
        name: 'Mercedes-Benz E220',
        cost: '35.00',
        ENGINE: '2200',
        INTERIOR_COLOR: 'Black',
        POWER: '190',
        FUEL_TYPE: 'Petrol',
        LENGTH: '4.5 meters',
        EXTERIOR_COLOR: 'Phantom Black',
        TRANSMISSION: 'Automatic',
        EXTRAS: 'Alloy Wheels, Bluetooth, Radio',
        bags: 4,
        persons: 4,
        details:
          'Do not worry about missing an important meeting because of road conditions, just relax in your reclining leather seat. A great choice for a business meeting or a business trip.',
        photo: 'https://i.ibb.co/JdNpg1L/e220.jpg',
      },
      {
        id: 2,
        name: 'BMW 5 Series Long',
        cost: '18.00',
        ENGINE: '3000',
        INTERIOR_COLOR: 'Black',
        POWER: '285',
        FUEL_TYPE: 'Diesel',
        LENGTH: '5.1 meters',
        EXTERIOR_COLOR: 'Silver',
        TRANSMISSION: 'Manual',
        EXTRAS: 'Leather Seats, LED Lighting, Radio',
        bags: 4,
        persons: 4,
        // tslint:disable-next-line:max-line-length
        details:
          'The mid-size luxury sedan by BMW with great V8 engine with both natural aspiration and turbocharging. A great option for corporate travel.',
        photo: 'https://i.ibb.co/chkDQpV/BMW-5-Series-Long.jpg',
      },
      {
        id: 3,
        name: 'Mercedes-Benz S600',
        cost: '47.50',
        ENGINE: '3200',
        INTERIOR_COLOR: 'Black',
        POWER: '285',
        FUEL_TYPE: 'Petrol',
        LENGTH: '5.1 meters',
        EXTERIOR_COLOR: 'Phantom Black',
        TRANSMISSION: 'Manual',
        EXTRAS: 'Air Conditioning, Bar, Heated Seats, Radio',
        bags: 6,
        persons: 4,
        details:
          'Comfortable Van for 7 passengers with plenty of leg room and a spacious trunk.',
        photo: 'https://i.ibb.co/NpxCFXz/Mercedes-Benz-S600.jpg',
      },
      {
        id: 4,
        name: 'Ford Tourneo',
        cost: '62.00',
        ENGINE: '3000',
        INTERIOR_COLOR: 'Laser Blue',
        POWER: '220',
        FUEL_TYPE: 'Diesel',
        LENGTH: '5.6 meters',
        EXTERIOR_COLOR: 'Carbon Gray',
        TRANSMISSION: 'Manual',
        EXTRAS: 'Air Conditioning, Alloy Wheels, LED Lighting, Radio',
        bags: 6,
        persons: 7,
        details:
          'Comfortable Van for 7 passengers with plenty of leg room and a spacious trunk.\n\n',
        photo: 'https://i.ibb.co/mGQmMkg/Ford-Tourneo.jpg',
      },
      {
        id: 5,
        name: 'Audi A8',
        cost: '85.00',
        ENGINE: '2800',
        INTERIOR_COLOR: 'Black',
        POWER: '240',
        FUEL_TYPE: 'Diesel',
        LENGTH: '5.1 meters',
        EXTERIOR_COLOR: 'Carbon Gray',
        TRANSMISSION: 'Automatic',
        EXTRAS: 'Bar, Bluetooth, Heated Seats, Sunroof',
        bags: 6,
        persons: 4,
        // tslint:disable-next-line:max-line-length
        details:
          'The most sophisticated sedan by Audi. The long version so when you sit back in the soft leather seats and stretch your legs you will be treated to an abundance of legroom. Relax with electronically controlled side and rear sunshades in the summer, and heated rear seats in the winter.',
        photo: 'https://i.ibb.co/jL2y14D/Audi-A8.jpg',
      },
      {
        id: 6,
        name: 'Mercedes-Benz S63 AMG',
        cost: '125.00',
        ENGINE: '5000',
        INTERIOR_COLOR: 'Black',
        POWER: '420',
        FUEL_TYPE: 'Petrol',
        LENGTH: '5.1 meters',
        EXTERIOR_COLOR: 'Carbon Gray',
        TRANSMISSION: 'Automatic',
        EXTRAS: 'Air Conditioning, Bar, Heated Seats, Sunroof',
        bags: 4,
        persons: 4,
        // tslint:disable-next-line:max-line-length
        details:
          'Powerful 5 liter engine and stunning power will take you on an unforgettable journey. Comfortable leather seats, excellent soundproofed cabin and a nice hum of the engine - it\'s AMG.',
        photo: 'https://i.ibb.co/d6n5LbC/Mercedes-Benz-S63-AMG.jpg',
      },
      {
        id: 7,
        name: 'Mercedes-Benz Maybach',
        cost: '165.00',
        ENGINE: '4000',
        INTERIOR_COLOR: 'Pink',
        POWER: '485',
        FUEL_TYPE: 'Petrol',
        LENGTH: '6 meters',
        EXTERIOR_COLOR: 'Pure White',
        TRANSMISSION: 'Automatic',
        EXTRAS:
          'Air Conditioning, Alloy Wheels, Bar, Bluetooth, LED Lighting, Sunroof',
        bags: 7,
        persons: 3,
        // tslint:disable-next-line:max-line-length
        // tslint:disable-next-line:max-line-length
        details:
          'This car unites the perfection of luxury, driving and comfort. Sneak up on an incredibly comfortable sofa and enjoy the trip.',
        photo: 'https://i.ibb.co/NFNhdrn/Mercedes-Benz-Maybach.jpg',
      },
      {
        id: 8,
        name: 'Cadillac Escalade SUV',
        cost: '175.00',
        ENGINE: '2200',
        INTERIOR_COLOR: 'Black',
        POWER: '325',
        FUEL_TYPE: 'Diesel',
        LENGTH: '6 meters',
        EXTERIOR_COLOR: 'Phantom Black',
        TRANSMISSION: 'Automatic',
        EXTRAS: 'Leather Seats, Radio, Sunroof',
        bags: 5,
        persons: 6,
        // tslint:disable-next-line:max-line-length
        details:
          'Smaller than the Cadillac Escalade Limousine but just as comfortable with a very powerful engine will give you the pleasure of traveling.',
        photo: 'https://i.ibb.co/xJCNkXr/Cadillac-Escalade-SUV.jpg',
      },
      {
        id: 9,
        name: 'Mercedes-Benz S600 Stretch',
        cost: '195.00',
        ENGINE: '5700',
        INTERIOR_COLOR: 'Black',
        POWER: '345',
        FUEL_TYPE: 'Petrol',
        LENGTH: '6 meters',
        EXTERIOR_COLOR: 'Phantom Black',
        TRANSMISSION: 'Manual',
        EXTRAS: 'Air Conditioning, Heated Seats, Sunroof',
        bags: 8,
        persons: 5,
        details:
          'The longest version of the luxury limousine from Mercedes. It provides superb comfort of traveling for 5 passengers.',
        photo: 'https://i.ibb.co/KswRg1T/Mercedes-Benz-S600-Stretch.jpg',
      },
      {
        id: 10,
        name: 'Cadillac Escalade Limousine',
        cost: '225.00',
        ENGINE: '6000',
        INTERIOR_COLOR: 'Black',
        POWER: '450',
        FUEL_TYPE: 'Petrol',
        LENGTH: '10 meters',
        EXTERIOR_COLOR: 'Phantom Black',
        TRANSMISSION: 'Automatic',
        EXTRAS: 'Bar, Leather Seats, LED Lighting',
        bags: 10,
        persons: 7,
        // tslint:disable-next-line:max-line-length
        details:
          'Cadillac Escalade is a unique limousine with a powerful engine and huge interior space for up to 7 passengers and a chauffeur. To further increase the comfort we recommend a maximum of 6 passengers.',
        photo: 'https://i.ibb.co/fMJzY0x/Cadillac-Escalade-Limousine.jpg',
      },
      {
        id: 11,
        name: 'Hummer New York Limousine',
        cost: '300.00',
        ENGINE: '5700',
        INTERIOR_COLOR: 'Laser Blue',
        POWER: '380',
        FUEL_TYPE: 'Diesel',
        LENGTH: '13 meters',
        EXTERIOR_COLOR: 'Pure White',
        TRANSMISSION: 'Manual',
        EXTRAS: 'Bar, Leather Seats, LED Lighting',
        bags: 5,
        persons: 12,
        // tslint:disable-next-line:max-line-length
        details:
          'Enter our 12-passenger limousine and step into an atmosphere of great luxury. Whether it is a wedding, anniversary or a night out with friends, this car is the perfect way to make it memorable.',
        photo: 'https://i.ibb.co/K9Sf6GX/Hummer-New-York-Limousine.jpg',
      },
    ];
  }

  getAllvehicles() {
    return [
      {label: 'Limousine', value: 'limousine'},
      {label: 'Sedan', value: 'sedan'},
      {label: 'Stretch Limousine', value: 'stretch-limousine'},
      {label: 'SUV', value: 'suv'},
      {label: 'Van', value: 'van'},
    ];
  }
} //end of CLass
