import {Injectable} from '@angular/core';
import {MapCollection} from '../../@shared/models/googleMaps';

@Injectable({
  providedIn: 'root',
})
export class MapsMatrixService {
  constructor() {
  }

  // tslint:disable-next-line:typedef
  calculateAndDisplayRoute(
    directionsService: google.maps.DirectionsService,
    directionsRenderer: google.maps.DirectionsRenderer,
    mapCollection: MapCollection
  ) {

    let waypts: google.maps.DirectionsWaypoint[] = [];
    waypts = this.fillWayPointsArray(mapCollection.wayPoints, waypts);
    if (mapCollection.originLocation !== '' && mapCollection.destinationLocation !== '') {
      directionsService.route(
        {
          origin: this.checkForUnDefinedArray(mapCollection.originLocation),
          destination: this.checkForUnDefinedArray(mapCollection.destinationLocation),
          // tslint:disable-next-line:triple-equals
          waypoints: this.checkForUnDefinedArray(waypts),
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING,
        },
        (response, status) => {
          if (status === 'OK') {
            directionsRenderer.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        }
      );
    }
  }// end of calculateAndDisplayRoute
  // tslint:disable-next-line:typedef
  fillWayPointsArray(points, returnArray) {
    const checkboxArray = points;
    // tslint:disable-next-line:triple-equals
    if (checkboxArray.length > 0 && points != undefined) {
      // tslint:disable-next-line:prefer-for-of
      checkboxArray.forEach((elm) => {
        returnArray.push({
          location: elm,
          stopover: true,
        });
      });
    }
    return returnArray;
  }// end of fillWayPointsArray
  // tslint:disable-next-line:typedef
  checkForUnDefinedArray(points) {
    console.log(points);
    if (points === undefined) {
      return [];
    }
    return points;
  }// checkForUnDefinedArray


  // tslint:disable-next-line:typedef
  calcDistanceAndTime(distanceMatrix, mapCollection: MapCollection) {
    const that = this;
    // @ts-ignore
    this.distanceMatrix.getDistanceMatrix(
      {
        travelMode: google.maps.TravelMode.DRIVING,
        origins: [mapCollection.originLocation, ...mapCollection.wayPoints],
        destinations: [mapCollection.destinationLocation],
        unitSystem: google.maps.UnitSystem.METRIC,
        // transitOptions: TransitOptions,
        // drivingOptions: DrivingOptions,
        // unitSystem: UnitSystem,
        // avoidHighways: Boolean,
        // avoidTolls: Boolean,
      },
      // tslint:disable-next-line:typedef
      function(response, status) {
        if (status !== google.maps.DistanceMatrixStatus.OK) {
          window.alert('Error was: ' + status);
        } else {
          console.log(response);
          const origins = response.originAddresses;
          for (let i = 0; i < origins.length; i++) {
            const results = response.rows[i].elements;
            // tslint:disable-next-line:prefer-for-of
            for (let j = 0; j < results.length; j++) {
              const element = results[j];
              if (element.status === 'OK') {
                this.resultDisTime.emit({
                  time: element.duration.text,
                  distance: element.distance,
                });
                // this.resultDisTime.emit({time: '', distance: element.distance});
                const distanceText = element.distance.text;
                const duration = element.duration.value / 60;
                const durationText = element.duration.text;
                // console.log(element.distance);
                // console.log(duration);
                // console.log(durationText);
                // if (duration) {
                //   atLeastOne = true;
                // }
                return element;
              }
            }
          }
        }
      }
    );


  }


} // end of CLass
