import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MyMapsService {
  constructor() {
  }

  getAvailableRoutes() {
    return [{label: 'New Your City Tour', value: 'new-york'},
      {label: 'Paris City Tour', value: 'paris'},
      {label: 'Cracow City Tour', value: 'cracow'},
      {label: 'San Daigo City Tour', value: 'san-daigo'},
      {label: 'London City Tour', value: 'london'},
    ];
  }

} //end of CLass
