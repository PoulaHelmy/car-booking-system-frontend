// tslint:disable-next-line:class-name
export interface mapMatrixRequest {
  // ref Link https://developers.google.com/maps/documentation/distance-matrix/overview
  origins;
  destinations;
  mode;
};

export interface MapCollection {
  originLocation;
  destinationLocation;
  wayPoints;
}
