export interface Cars {
  id: number;
  name: string;
  cost: string;
  ENGINE: string;
  INTERIOR_COLOR: string;
  POWER: string;
  FUEL_TYPE: string;
  LENGTH: string;
  EXTERIOR_COLOR: string;
  TRANSMISSION: string;
  EXTRAS: string;
  bags: number;
  persons: number;
  details: string;
  photo: string;
}
