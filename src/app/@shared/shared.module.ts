import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NotFoundComponent} from './pages/not-found/not-found.component';
import {MaterialModule} from './Material/Material.module';


@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    MaterialModule],
  exports: [NotFoundComponent, MaterialModule],
})
export class SharedModule {
}
