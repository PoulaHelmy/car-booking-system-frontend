import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BookingModule} from './booking/booking.module';
import {SharedModule} from './@shared/shared.module';
import {NgxAutocomPlaceModule} from 'ngx-autocom-place';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BookingModule,
    SharedModule,
    NgxAutocomPlaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
